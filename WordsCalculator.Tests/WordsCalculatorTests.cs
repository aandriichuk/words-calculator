﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WordsCalculatorTests.cs" company="SimCorp">
//   © 2017 ANDRII ANDRIICHUK ALL RIGHTS RESERVED
// </copyright>
// <summary>
//   Defines the WordsCalculatorTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsCalculator.Tests
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using Calculator;
    using FileParsers;
    using NUnit.Framework;

    /// <summary>
    /// The words calculator tests.
    /// </summary>
    [TestFixture]
    [Category("Words Calculator Unit Tests")]
    [Category("Unit")]
    public class WordsCalculatorTests
    {
        /// <summary>
        /// Gets the test cases.
        /// </summary>
        public static IEnumerable TestCases
        {
            get
            {
                yield return new TestCaseData("text-file.txt", new Dictionary<string, int> { { "Go", 1 }, { "do", 2 }, { "that", 2 }, { "thing", 1 }, { "you", 1 }, { "so", 1 }, { "well", 1 } }).SetName("Positive case");
                yield return new TestCaseData("text-file.txt", new Dictionary<string, int> { { "Not exist", 5 }, { "Go", 1 }, { "do", 2 }, { "that", 2 }, { "thing", 1 }, { "you", 1 }, { "so", 1 }, { "well", 1 } }).SetName("Negative case");
            }
        }

        /// <summary>
        /// The file path to text file.
        /// </summary>
        public string FilePath => AppDomain.CurrentDomain.BaseDirectory;

        /// <summary>
        /// Use it to initialize any things before execute test.
        /// </summary>
        [SetUp]
        public void Init()
        {
        }

        /// <summary>
        /// Validates that words occurrence amount is calculated correct.
        /// </summary>
        /// <param name="textFileName">
        /// The text file name.
        /// </param>
        /// <param name="expectedAmount">
        /// The expected amount.
        /// </param>
        [Test, TestCaseSource(nameof(TestCases))]
        public void WordsOccurrenceAmountIsValid(string textFileName, Dictionary<string, int> expectedAmount)
        {
            // ARRANGE
            var fileParser = new TextFileParser();
            var fileText = fileParser.Parse(string.Concat(this.FilePath, "\\", textFileName));
            var calculator = new FileCalculator();

            // ACT
            var calculatedAmount = calculator.CalculateWordOccurrence(fileText);

            // ASSERT
            Assert.AreEqual(calculatedAmount, expectedAmount);
        }
    }
}

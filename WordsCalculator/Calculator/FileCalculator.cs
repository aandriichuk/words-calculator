﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileCalculator.cs" company="SimCorp">
//   © 2017 ANDRII ANDRIICHUK ALL RIGHTS RESERVED
// </copyright>
// <summary>
//   Implements file calculator which allows calculate different things in text files.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsCalculator.Calculator
{
    using System.Collections.Generic;
    using System.Linq;
    using Interface;

    /// <summary>
    /// Implements file calculator.
    /// </summary>
    public class FileCalculator : ICalculator
    {
        /// <summary>
        /// Allows calculate word occurrence.
        /// </summary>
        /// <param name="textLines">
        /// The text lines.
        /// </param>
        /// <returns>
        /// Dictionary which contains words and amount of occurrence in text
        /// </returns>
        public Dictionary<string, int> CalculateWordOccurrence(IEnumerable<string> textLines)
        {
            return textLines.SelectMany(str => str.Split())
                .Where(str => str != string.Empty)
                .GroupBy(str => str)
                .ToDictionary(word => word.Key, word => word.Count());
        }
    }
}

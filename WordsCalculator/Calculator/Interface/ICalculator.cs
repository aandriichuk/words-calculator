﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICalculator.cs" company="SimCorp">
//  © 2017 ANDRII ANDRIICHUK ALL RIGHTS RESERVED
// </copyright>
// <summary>
//   Defines the ICalculator type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsCalculator.Calculator.Interface
{
    using System.Collections.Generic;

    /// <summary>
    /// The Calculator interface.
    /// </summary>
    public interface ICalculator
    {
        /// <summary>
        /// Allows calculate word occurrence.
        /// </summary>
        /// <param name="textLines">
        /// The text lines.
        /// </param>
        /// <returns>
        /// Dictionary which contains words and amount of occurrence in text
        /// </returns>
        Dictionary<string, int> CalculateWordOccurrence(IEnumerable<string> textLines);
    }
}

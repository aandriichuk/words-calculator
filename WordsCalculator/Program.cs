﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="SimCorp">
//   © 2017 ANDRII ANDRIICHUK ALL RIGHTS RESERVED
// </copyright>
// <summary>
//   Defines calculator which allows calculate amount of word occurence in a text file.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsCalculator
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Calculator;
    using Calculator.Interface;
    using FileParsers;
    using Managers;
    using Ninject;

    /// <summary>
    /// The program main class.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Represents IoC container
        /// </summary>
        private static readonly IKernel Kernel = new StandardKernel();

        /// <summary>
        /// The main program entry point.
        /// </summary>
        /// <param name="args">
        /// The parameters if needed.
        /// </param>
        internal static void Main(string[] args)
        {
            RegisterContainer();

            var fileManager = Kernel.Get<FileManager>();
            fileManager.Parse("text.txt");
            fileManager.DisplayWordOccurrence();

            Console.ReadLine();
        }

        /// <summary>
        /// Registers component's instances in Ninject container.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
        internal static void RegisterContainer()
        {
            Kernel.Bind<IParser>().To<TextFileParser>();
            Kernel.Bind<ICalculator>().To<FileCalculator>();
            Kernel.Bind<IFileManager>().To<FileManager>();
        }
    }
}

﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TextFileParser.cs" company="SimCorp">
//   © 2017 ANDRII ANDRIICHUK ALL RIGHTS RESERVED
// </copyright>
// <summary>
//   Implements the TextFileParser type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsCalculator.FileParsers
{
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// The text file parser.
    /// </summary>
    public class TextFileParser : IParser
    {
        /// <summary>
        /// Allows parse text.
        /// </summary>
        /// <param name="file">
        /// The file path.
        /// </param>
        /// <returns>
        /// List of text lines.
        /// </returns>
        public IEnumerable<string> Parse(string file)
        {
            return File.ReadLines(file);
        }
    }
}

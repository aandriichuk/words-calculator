﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IParser.cs" company="SimCorp">
//   © 2017 ANDRII ANDRIICHUK ALL RIGHTS RESERVED
// </copyright>
// <summary>
//   Defines the IParse type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsCalculator.FileParsers
{
    using System.Collections.Generic;

    /// <summary>
    /// An abstract parser.
    /// </summary>
    public interface IParser
    {
        /// <summary>
        /// Allows parse text.
        /// </summary>
        /// <param name="file">
        /// The file path.
        /// </param>
        /// <returns>
        /// List of text lines.
        /// </returns>
        IEnumerable<string> Parse(string file);
    }
}

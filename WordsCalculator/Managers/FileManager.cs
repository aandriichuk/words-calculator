﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileManager.cs" company="SimCorp">
//   © 2017 ANDRII ANDRIICHUK ALL RIGHTS RESERVED
// </copyright>
// <summary>
//   Defines the File Manager component.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsCalculator.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using Calculator.Interface;
    using FileParsers;

    /// <summary>
    /// Implements File Manager.
    /// </summary>
    public class FileManager : IFileManager
    {
        /// <summary>
        /// Represents a strategy to parse text file.
        /// </summary>
        private readonly IParser _parser;

        /// <summary>
        /// Contains a calculator instance.
        /// </summary>
        private readonly ICalculator _calculator;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileManager"/> class.
        /// </summary>
        /// <param name="parser">
        /// A parser instance which allows parse specific text file.
        /// </param>
        /// <param name="calculator">
        /// The calculator.
        /// </param>
        public FileManager(IParser parser, ICalculator calculator)
        {
            _parser = parser;
            _calculator = calculator;
        }

        /// <summary>
        /// Gets or sets the parsed lines.
        /// </summary>
        private IEnumerable<string> ParsedLines { get; set; }

        /// <summary>
        /// Provides ability to parse file.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        public void Parse(string file)
        {
            ParsedLines = _parser.Parse(file);
        }

        /// <summary>
        /// Allow display amount of word occurrence in console.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:PrefixLocalCallsWithThis", Justification = "Reviewed. Suppression is OK here.")]
        public void DisplayWordOccurrence()
        {
            var calculatedResult = _calculator.CalculateWordOccurrence(ParsedLines);
            foreach (var result in calculatedResult)
            {
                Console.WriteLine($"{result.Key.ToUpper()} occurs {result.Value} time(s)");
            }
        }
    }
}

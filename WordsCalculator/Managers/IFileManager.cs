﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IFileManager.cs" company="SimCorp">
//   © 2017 ANDRII ANDRIICHUK ALL RIGHTS RESERVED
// </copyright>
// <summary>
//   Defines the IFileManager type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsCalculator.Managers
{
    /// <summary>
    /// The FileManager interface.
    /// </summary>
    public interface IFileManager
    {
        /// <summary>
        /// Provides ability to parse file.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        void Parse(string file);

        /// <summary>
        /// Allow display amount of word occurrence in console.
        /// </summary>
        void DisplayWordOccurrence();
    }
}
